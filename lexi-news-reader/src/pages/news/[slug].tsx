import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import { useTranslation } from "next-i18next";
import Link from "next/link";

export default function NewsDetail({ data }) {
  const { t } = useTranslation();

  return (
    <div className="mt-6 py-6 px-20">
      <div className="flex justify-between">
        <Link
          href="/"
          className="underline underline-offset-2 underine-green hover:no-underline text-green-900"
        >
          ⏎ {t("homepage_nav_link_label")}
        </Link>
        <p className="font-mono bg-green-100 px-2 w-fit">{data.date}</p>
      </div>

      <article className="container">
        <h1 className="mt-6 text-4xl font-bold">{data.title}</h1>
        <div className="mt-6">
          <p>{data.content}</p>
        </div>
      </article>
    </div>
  );
}

// We destructure our `locales` array from the
// context parameter object.
export const getStaticPaths = async ({ locales }) => {
  // Grab the news data from our mock backend for
  // our default locale.
  const res = await fetch("http://localhost:3001/");
  const data = await res.json();

  // Create an array of path objects, one for each
  // locale + news item. For example, if we had
  // two news item with slugs 'foo' and 'bar', our
  // `paths` array would contain four entries:
  //   - { params: { slug: 'foo' }, locale: 'en' }
  //   - { params: { slug: 'foo' }, locale: 'es' }
  //   - { params: { slug: 'bar' }, locale: 'en' }
  //   - { params: { slug: 'bar' }, locale: 'es' }
  const paths = data.flatMap((news) => {
    return locales.map((locale) => ({
      params: {
        slug: news.slug,
      },
      locale,
    }));
  });

  return {
    paths,

    // All our news pages are pre-built and any path *not*
    // declared above results in a 404 (Not Found) error
    // page.
    fallback: false,
  };
};

export const getStaticProps = async (context) => {
  const { slug } = context.params;
  const { locale } = context;

  const res = await fetch(`http://localhost:3001/${locale}/${slug}`);
  const data = await res.json();

  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      data,
    },
  };
};
