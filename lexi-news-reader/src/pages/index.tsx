import { Inter } from "next/font/google";
import { GetServerSidePropsContext } from "next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Head from "next/head";
import { useTranslation } from "next-i18next";
import Link from "next/link";

const inter = Inter({ subsets: ["latin"] });

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const { locale } = context;
  const res = await fetch(`http://localhost:3001/${locale}`);
  const data = await res.json();
  return {
    props: {
      ...(await serverSideTranslations(locale ?? "", ["common"])),
      data,
      locale,
    },
  };
};

type HomeProps = {
  data: any;
  locale: string;
};

export default function Home(props: HomeProps) {
  const { t } = useTranslation();
  return (
    <div>
      <Head>
        <title>{t("app_title")}</title>
      </Head>

      <div className="grid grid-rows-4 p-4 md:grid-rows-2 md:p-0 grid-flow-col gap-4">
        {props.data.map((news: any, index: number) => (
          <div
            key={index}
            className="mt-6 p-6 border-2 border-green-100 hover:border-green-200 shadow-lg hover:shadow-md rounded-md flex flex-col gap-4"
          >
            <p className="font-mono bg-green-100 px-2 w-fit rounded-md">
              {news.date}
            </p>

            <Link
              href={{
                pathname: "/news/[slug]",
                query: { slug: news.slug },
              }}
              locale={props.locale}
            >
              <h3>{news.title}</h3>
            </Link>

            <p>{news.description}</p>
            <Link
              href={{
                pathname: "/news/[slug]",
                query: { slug: news.slug },
              }}
              locale={props.locale}
            >
              {t("button_label")}
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
}
