# INSTRUCTIONS

Open two terminals.

# Terminal 1 - Backend

```bash
cd backend
npm i
node index.js
```

# Terminal 2 - Frontend

```bash
cd lexi-news-reader
npm i
npm run dev
```
